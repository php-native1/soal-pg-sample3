CREATE TABLE `t_soal` (
 
`no` int(11) NOT NULL AUTO_INCREMENT,
 
`soal` text NOT NULL,
 
`a` varchar(1000) NOT NULL,
 
`b` varchar(1000) NOT NULL,
 
`c` varchar(1000) NOT NULL,
 
`d` varchar(1000) NOT NULL,
 
`kunci` varchar(1000) NOT NULL,
 
PRIMARY KEY (`no`)
 
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
 
-- ----------------------------
 
-- Records
 
-- ----------------------------
 
INSERT INTO `t_soal` VALUES ('1', 'Siapa presiden pertama Indonesia', 'Soekarno', 'Suharto', 'Ki Hajar Dewantara', 'Megawati Soekarno Puteri', 'Soekarno');
 
INSERT INTO `t_soal` VALUES ('2', 'Apa kepanjangan dari NKRI', 'Negara Kesatuan Rakyat Indonesia', 'Negara Kesatuan Republik Indonesia', 'Negara Kedaulatan Republik Indonesia', 'Negara Kekeluargaan Rakyat Indonesia', 'Negara Kesatuan Republik Indonesia');
 
INSERT INTO `t_soal` VALUES ('3', '2 + 2 = ', '10:3', '8:4', '(2:2)x(2*2)', '7:3', '(2:2)x(2*2)');
 
INSERT INTO `t_soal` VALUES ('4', 'UPI merupakan singkatan dari', 'Universitas Padahal IKIP', 'Universitas Pendidikan Indonesia', 'Universitas Pelawak Internasional', 'Semua Benar', 'Universitas Pendidikan Indonesia');
 
INSERT INTO `t_soal` VALUES ('5', 'Siapa nama ayah Arum Yuniarsih', 'Ruman', 'Jajang', 'Dedi', 'Sarbini', 'Ruman');
 
INSERT INTO `t_soal` VALUES ('6', 'Malaikat pencabut nyawa adalah malaikat', 'Izroil', 'Jibril', 'Mikail', 'Isrofil', 'Izroil');

