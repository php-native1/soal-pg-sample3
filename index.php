<?php
 
    // koneksi ke mysqli
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $db = "db_soal2";
    // Create connection
    $koneksi = mysqli_connect($servername, $username, $password,$db);
    // Check connection
    if (!$koneksi) {
    die("Connection failed: " . mysqli_connect_error());
    }
 
    session_start();
 
    $query = mysqli_query($koneksi, "select * from t_soal") or die (mysqli_connect_error());
 
    //$_SESSION['soal'] = mysql_fetch_array($query);
 
    $_SESSION['soal'] = array();
 
    $_SESSION['no'] = 1;
 
    $_SESSION['score'] = 0;
 
    $_SESSION['option'] = array();
 
    $_SESSION['jawab'] = array();
 
    $i=0;
 
    while($row = mysqli_fetch_array($query)){
 
        $_SESSION['soal'][] = $row;
 
        $_SESSION['option'][] = array($_SESSION['soal'][$i]['a'], $_SESSION['soal'][$i]['b'], $_SESSION['soal'][$i]['c'], $_SESSION['soal'][$i]['d']);
 
        $i++;
 
    }
 
    if(isset($_SESSION['soal'])){
 
        header("location:test.php");
 
    }
 
?>